% setup model parameters
% input argument: Q: scalar: number of virus strains
% input argument: J: number of T cell pools
% output argument: p: struct array: parameters
% output argument: iv: vector: initial values
% output argument: c: struct array: compartment labels
function [p,iv,c] = setup_parameters(Q,J)

% parameter values
p.beta = 5e-7*ones(Q,1);
p.g = .8;

p.pV = 210/5e-7*3e-8*ones(Q,1);
p.pF = 1e-5*ones(Q,1);
p.pA = .8*ones(Q,1);

p.deltaI = 2*ones(Q,1);
p.deltaV = 5*ones(Q,1);
p.deltaF = 2;
p.deltaE = 0.6*ones(J,1);
p.deltaEhat = 0.6*ones(J,1);
p.deltaB = .1*ones(Q,1);
p.deltaA = .04*ones(Q,1);
p.epsE = .02*ones(J,1);

crkappa = ones(Q,J);
p.kappaE = 3e-5*crkappa;
p.kappaA = 3*ones(Q,1);
p.kappaF =2.5*ones(Q,1);

p.kB = 2e5*ones(Q,1);
crk= ones(Q,J);
p.kC = 5e6./crk;

p.tauB = 3*ones(Q,1);
p.tauE = 6*ones(J,1);
p.tauR = 14*ones(J,1);

p.nE = 20;
p.nB = 5;

p.T_0 = 7e7;

% compartment labels
c.T = 1; 
c.I = c.T+1:c.T+Q; 
c.V = c.I(end)+1:c.I(end)+Q; 
c.F = c.V(end)+1; 
c.C = c.F+1:c.F+J;
c.E = reshape(c.C(end)+1:c.C(end)+p.nE*J,p.nE,J);
c.M = c.E(end,end)+1:c.E(end,end)+J;
c.Chat = c.M(end)+1:c.M(end)+J;
c.Ehat = reshape(c.Chat(end)+1:c.Chat(end)+p.nE*J,p.nE,J);
c.B = reshape(c.Ehat(end,end)+1:c.Ehat(end,end)+(p.nB+1)*Q,p.nB+1,Q);
c.P = c.B(end,end)+1:c.B(end,end)+Q;
c.A = c.P(end,end)+1:c.P(end,end)+Q;

% initial values -- V_0 to be added later
iv = zeros(1,c.A(end));
iv(c.T) = p.T_0;
iv(c.B(1,:)) = 10;
iv(c.C) = 100;
end