% produce summary statistics given parameters, initial conditions and
% inter-exposure interval
% input argument: p: struct array: parameters set up using setup_parameters
% input argument: iv: 1*n array: initial values set up using setup_parameters
% input argument: c: struct array: compartment labels set up using setup_parameters
% input argument: V_0: 1*2 vector: initial amount of virus 1 and amount of
% virus 2 added upon exposure
% input argument: interval: scalar: inter-exposure interval
% output argument: recovery_time: 1*2 vector: recovery time
% output argument: peak_time: 1*2 vector: time to peak viral load
% output argument: peak_viral_load: 1*2 vector: peak viral load
% output argument: auc_V: 1*2 vector: area under viral load curve
% output argument: total_T: scalar: total number of T cells at end of
% second infection
% output argument: iei_T: scalar: total number of T cells just before
% second infection
% output argument: sol_out: struct array containing fields
% x: 1*m array: time array of solution of ODEs
% V: 2*m array: viral load trajectory
% T: 1*m array: trajectory of total number of T cells
function [recovery_time,peak_time,peak_viral_load,auc_V,total_T,iei_T,sol_out] =...
    summary_statistics(p,iv,c,V_0,interval)

% initialise arrays
Q = length(c.V);
J = length(c.C);
recovery_time = zeros(2,1);
peak_time = recovery_time;
peak_viral_load = recovery_time;
auc_V = recovery_time;

% default model settings
end_integrate_time = interval + 100;
continue_after_end_infection = 1;

% solve system of ODEs
sol = solve_ODE(p,iv,c,interval,...
    V_0,end_integrate_time,continue_after_end_infection);

sol_out.x = sol.x;
sol_out.V = sol.y(c.V,:);

% recovery time
stopping_point = find(diff(sol.x) == 0);
recovery_time(1) = sol.x(stopping_point(1));
recovery_time(2) = sol.x(stopping_point(end))-interval;
iei_time = find(sol.x == interval);

if(J == 1)
    % total number of T cells at end of second infection
    total_T = sol.y(c.C,end)+sum(sol.y(c.E,end))...
        +sol.y(c.M,end)+sol.y(c.Chat,end)+sum(sol.y(c.Ehat,end));
    % total number of T cells at inter-exposure interval
    iei_T = sol.y(c.C,iei_time(1))+sum(sol.y(c.E,iei_time(1)))...
        +sol.y(c.M,iei_time(1))+sol.y(c.Chat,iei_time(1))+sum(sol.y(c.Ehat,iei_time(1)));
else
    % total number of T cells at end of second infection
    total_T = sum(sol.y(c.C,end))+sum(sol.y(c.E,end))...
        +sum(sol.y(c.M,end))+sum(sol.y(c.Chat,end))+sum(sol.y(c.Ehat,end));
    % total number of T cells at inter-exposure interval
    iei_T = sum(sol.y(c.C,iei_time(1)))+sum(sol.y(c.E,iei_time(1)))...
        +sum(sol.y(c.M,iei_time(1)))+sum(sol.y(c.Chat,iei_time(1)))+sum(sol.y(c.Ehat,iei_time(1)));
end

% peak viral load
for m = 1:Q
    [peak_viral_load(m),peak_loc] = max(sol.y(c.V(m),:));
end
% time at which peak viral load is attained
peak_time(1) = sol.x(peak_loc);
peak_time(2) = sol.x(peak_loc) - interval;

% T cell trajectory
if(J == 1)
    sol_out.T = sol.y(c.C,:)+sum(sol.y(c.E,:))...
        +sol.y(c.M,:)+sol.y(c.Chat,:)+sum(sol.y(c.Ehat,:));
else
    sol_out.T = sum(sol.y(c.C,:))+sum(sol.y(c.E,:))...
        +sum(sol.y(c.M,:))+sum(sol.y(c.Chat,:))+sum(sol.y(c.Ehat,:));
end

% area under viral load curve
for m = 1:Q
    auc_V(m) = trapz(sol.x,sol.y(c.V(m),:));
end

end

% function to solve ODE model
% input argument: p: struct array set up using setup_parameters
% input argument: iv: 1*n vector: initial values set up using setup_parameters
% input argument: c: struct array: compartment labels set up using setup_parameters
% input argument: interval: inter-exposure interval (set to 0 if integratng
% one strain only)
% input argument: V_0: 1*Q vector: initial amount of virus 1 and amount of
% virus 2 added upon exposure (if applicable)
% input argument: end_integrate_time: integrate from t = 0 to t =
% end_integrate_time
% input argument: continue_after_end_infection: logical: whether to keep
% integrating after all virus becomes extinct
% output argument: sol: strucy array containing the fields
% x: 1*m vector: time series
% y: n*m array: values of compartments at times x
function sol = solve_ODE(p,iv,c,interval,...
    V_0,end_integrate_time,continue_after_end_infection)

Q = length(c.V); % number of strains

iv(c.V(1)) = V_0(1); % initialise

% ODE solving options
threshold = 1;
% no event detection
options = odeset('NonNegative',1:length(iv));
% detect strain 1 going below threshold
options_strain1 = odeset('NonNegative',1:length(iv),'Events',@(t,y)less_than_threshold(t,y,c,1,threshold));
% detect strain 2 going below threshold
options_strain2 = odeset('NonNegative',1:length(iv),'Events',@(t,y)less_than_threshold(t,y,c,2,threshold));
% detect either strain going below threshold
options_strain12 = odeset('NonNegative',1:length(iv),...
    'Events',@(t,y)less_than_threshold(t,y,c,[1,2],threshold));

if(Q == 1)
    interval = end_integrate_time; % for single infection, solve until end time
end

% solve until second virus is introduced
sol = ode15s(@(t,x) ODE_wrapper(t,x,p,c,1),[0,interval],iv,options_strain1);

if(Q == 2)
    if(isempty(sol.xe)) % if first infection is not below the threshold by the time the second virus is introduced
        iv = sol.y(:,end);
        iv(c.V(2)) = V_0(2); % add second virus
        sol2 = ode15s(@(t,x) ODE_wrapper(t,x,p,c,[1,2]),[interval,end_integrate_time],iv,options_strain12);
        sol = structcat(sol,sol2);
        if(~isempty(sol2.xe)) % if one virus goes below the threshold
            % determine which it is
            if(sol2.ye(c.V(1)) < threshold && sol2.ye(c.I(1)) < threshold)
                strain_below = 1;
                strain_above = 2;
            else
                strain_above = 1;
                strain_below = 2;
            end
            
            iv = sol2.ye;
            iv([c.I(strain_below),c.V(strain_below)]) = 0; % set the virus below the threshold to zero
            sol2 = ode15s(@(t,x) ODE_wrapper(t,x,p,c,strain_above),[sol2.xe,end_integrate_time],iv,options_strain2); % continue solving for the other virus
            sol = structcat(sol,sol2);
            if(continue_after_end_infection && ~isempty(sol2.xe)) % continue solving after both viruses are below the threshold if desired
                iv = sol2.ye;
                iv([c.I(strain_above),c.V(strain_above)]) = 0; % set the other virus to zero
                sol2 = ode15s(@(t,x) ODE_wrapper(t,x,p,c,[]),[sol2.xe,end_integrate_time],iv);
                sol = structcat(sol,sol2);
            end
        end
    else % if the first virus is below the threshold before the second infection
        iv = sol.ye;
        iv([c.I(1),c.V(1)]) = 0; % set the first virus to zero
        sol2 = ode15s(@(t,x) ODE_wrapper(t,x,p,c,[]),[sol.xe,interval],iv,options); % solve until the second virus is introduced
        sol = structcat(sol,sol2);
        
        iv = sol2.y(:,end);
        iv(c.V(2)) = V_0(2); % introduce the second virus
        sol2 = ode15s(@(t,x) ODE_wrapper(t,x,p,c,2),[interval,end_integrate_time],iv,options_strain2);
        sol = structcat(sol,sol2);
        if(continue_after_end_infection && ~isempty(sol2.xe)) % keep solving after the second virus is below the threshold if desired
            iv = sol2.ye;
            iv([c.I(2),c.V(2)]) = 0;
            sol2 = ode15s(@(t,x) ODE_wrapper(t,x,p,c,[]),[sol2.xe,end_integrate_time],iv);
            sol = structcat(sol,sol2);
        end
    end
end

end

% concatenate two ODE solutions
% input argument: b: struct array with fields
%                   x: 1*m1 vector
%                   y: n*m1 array
% input argument: c: struct array with fields
%                   x: 1*m2 vector
%                   y: n*m2 array
% output argument: a: struct array with fields
%                   x: 1*(m1+m2) vector
%                   y: n*(m1+m2) array

function a = structcat(b,c)
a.x = horzcat(b.x,c.x);
a.y = horzcat(b.y,c.y);
end

% Locate the time when both the number of infected cells and virus for the
% given strains(s) goes below the threshold, and stop integration
% input argument: y: n*1 vector: compartment values at time
% input argument: c: struct array: compartment labels
% input arguments: strain: 1*m vector: strains for which threshold is to be
% triggered
% input argument: threshold: scalar: terminate if both viral load and
% infected cell count of strains included in strain argument are below
% threshold
% output argument: value: scalar: whether viral load and/or infected cell
% count is above threshold
% output argument: isterminal: logical: whether to stop integration when
% value goes through 0
% output argument: direction: scalar: -1 indicates to stop integration only
% when going from above threshold to below threshold
function [value,isterminal,direction] = less_than_threshold(~,y,c,strain,threshold)

if(length(strain) == 1)
    value = double(y(c.I(strain)) > threshold || y(c.V(strain)) > threshold);
else
    strain_1_gt_threshold = y(c.I(strain(1))) > threshold || y(c.V(strain(1))) > threshold;
    strain_2_gt_threshold = y(c.I(strain(2))) > threshold || y(c.V(strain(2))) > threshold;
    value = double(strain_1_gt_threshold && strain_2_gt_threshold);
end
isterminal = 1;   % Stop the integration
direction = -1;   % Only when going from above to below threshold
end

% wrapper for RHS of ODE which
% sets derivative of infected cell and virion count for selected strain(s)
% to 0
% input argument: x: n*1 vector: compartment values
% input argument: p: struct array: parameters
% input argument: c: struct array: compartment labels
% input argument: strains: 1*m vector: strains to set to 0
% output argument: xdot: n*1 vector: derivatives
function xdot = ODE_wrapper(~,x,p,c,strains)

xdot = ODEmodel(x,p,c);
exc_strains = 1:2;
exc_strains(strains) = [];
xdot([c.V(exc_strains),c.I(exc_strains)]) = 0;
end

% model equations
% input argument: x: n*1 vector: compartment values
% input argument: p: struct array: parameters
% input argument: c: struct array: compartment labels
% output argument: xdot: n*1 vector: derivatives
function xdot = ODEmodel(x,p,c)

xdot = zeros(c.A(end),1);
xdot(c.T) = p.g*(p.T_0 - x(c.T) - sum(x(c.I)))/p.T_0*x(c.T)...
    - (dot(p.beta,x(c.V)))*x(c.T);
xdot(c.I) = p.beta.*x(c.V)*x(c.T) - (p.deltaI+p.kappaF*x(c.F)+p.kappaE*(sum(x(c.E))+sum(x(c.Ehat)))').*x(c.I);
xdot(c.V) = p.pV.*x(c.I) - (p.deltaV+p.kappaA.*x(c.A)+p.beta*x(c.T)).*x(c.V);
xdot(c.F) = dot(p.pF,x(c.I)) - p.deltaF*x(c.F);

sum_stimC = (x(c.I)'*(1./p.kC))';
xdot(c.C) = -sum_stimC./(1+sum_stimC).*x(c.C);
xdot(c.E(1,:)) = sum_stimC./(1+sum_stimC).*x(c.C) - p.deltaE.*x(c.E(1,:)) - x(c.E(1,:))./p.tauE*p.nE;
for i = 2:p.nE-1
    xdot(c.E(i,:)) = (2*x(c.E(i-1,:))-x(c.E(i,:)))./p.tauE*p.nE - p.deltaE.*x(c.E(i,:));
end
xdot(c.E(end,:)) = 2*x(c.E(end-1,:))./p.tauE*p.nE-p.deltaE.*x(c.E(end,:));
xdot(c.M) = p.epsE.*p.deltaE.*x(c.E(end,:)) + p.epsE.*p.deltaEhat.*x(c.Ehat(end,:)) - x(c.M)./p.tauR;

xdot(c.Chat) = x(c.M)./p.tauR - sum_stimC./(1+sum_stimC).*x(c.Chat);
xdot(c.Ehat(1,:)) = sum_stimC./(1+sum_stimC).*x(c.Chat) - p.deltaEhat.*x(c.Ehat(1,:)) - x(c.Ehat(1,:))./p.tauE*p.nE;
for i = 2:p.nE-1
    xdot(c.Ehat(i,:)) = (2*x(c.Ehat(i-1,:))-x(c.Ehat(i,:)))./p.tauE*p.nE - p.deltaEhat.*x(c.Ehat(i,:));
end
xdot(c.Ehat(end,:)) = 2*x(c.Ehat(end-1,:))./p.tauE*p.nE-p.deltaEhat.*x(c.Ehat(end,:));

xdot(c.B(1,:)) = -x(c.V)./(p.kB+x(c.V)).*x(c.B(1,:));
xdot(c.B(2,:)) = -xdot(c.B(1,:)) - x(c.B(2,:))./p.tauB*p.nB - p.deltaB.*x(c.B(2,:));
for i = 3:p.nB+1
    xdot(c.B(i,:)) = (2*x(c.B(i-1,:))-x(c.B(i,:)))./p.tauB*p.nB- p.deltaB.*x(c.B(i,:));
end
xdot(c.P) = 2*x(c.B(end,:))./p.tauB*p.nB-p.deltaB.*x(c.P);
xdot(c.A) = p.pA.*x(c.P) - p.deltaA.*x(c.A);

end


