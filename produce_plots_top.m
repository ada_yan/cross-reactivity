% function to produce all plots in manuscript
function produce_plots_top()

produce_plots; % figures in main text

% figures in sensitivity analysis
pV = horzcat([.4:.2:1],2:8)*12.6;
produce_plots_sensitivity('pV',pV,12.6)

pF = 10.^(-2:.5:1)*1e-5;
produce_plots_sensitivity('pF',pF,1e-5)

kappaA = 10.^(-1:8)*3;
produce_plots_sensitivity('kappaA',kappaA,3)
end

% function to produce all figures in main text
function produce_plots()

dir_string = 'figs/'; % output directory for figures

if(~exist(dir_string,'dir'))
    mkdir(dir_string);
end

fh = plot_fns;

summary_figure(strcat(dir_string,'3'));

Q = 2;
J = 1;
xreact = (0:.1:1)';
fixed_interval = 100;
intervals = [1,3,5,7,10,14,20:5:50];
V_0 = [10,10];

C0_number = 100;
% solutions for different avidities
recovery_time_avidity = zeros(length(xreact),Q);
total_T_avidity = zeros(length(xreact),J);
iei_T_avidity = total_T_avidity;

for i = 1:length(xreact)
    [p,iv,c] = setup_parameters(Q,J);
    ad = xreact(i)*ones(Q,J);
    p.kappaE = p.kappaE.*ad;
    p.kC = p.kC./ad;
    [recovery_time_avidity(i,:),~,~,~,total_T_avidity(i),iei_T_avidity(i),~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different precursor frequencies
recovery_time_precursor_frequency = recovery_time_avidity;
total_T_precursor_frequency = total_T_avidity;
iei_T_precursor_frequency = total_T_avidity;

for i = 1:length(xreact)
    [p,iv,c] = setup_parameters(Q,J);
    iv(c.C) = xreact(i)*C0_number;
    [recovery_time_precursor_frequency(i,:),~,~,~,total_T_precursor_frequency(i),iei_T_precursor_frequency(i),~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different epitope abundances
recovery_time_abundance_change1 = recovery_time_avidity;
total_T_abundance_change1 = total_T_avidity;
iei_T_abundance_change1 = total_T_avidity;

recovery_time_abundance_change2 = recovery_time_avidity;
total_T_abundance_change2 = total_T_avidity;
iei_T_abundance_change2 = total_T_avidity;

for i = 1:length(xreact)
    [p,iv,c] = setup_parameters(Q,J);
    ad = [xreact(i);1];
    p.kappaE = p.kappaE.*ad;
    p.kC = p.kC./ad;
    [recovery_time_abundance_change1(i,:),~,~,~,total_T_abundance_change1(i),iei_T_abundance_change1(i),~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
    
    [p,iv,c] = setup_parameters(Q,J);
    ad = [1;xreact(i)];
    p.kappaE = p.kappaE.*ad;
    p.kC = p.kC./ad;
    [recovery_time_abundance_change2(i,:),~,~,~,total_T_abundance_change2(i),iei_T_abundance_change2(i),~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different inter-exposure intervals with cross-reactivity
% and memory

recovery_time_aa = zeros(length(intervals),2);
peak_time_aa = recovery_time_aa;
peak_viral_load_aa = recovery_time_aa;
auc_V_aa = recovery_time_aa;
total_T_aa = zeros(length(intervals),1);
sol_aa = cell(length(intervals),1);

[p,iv,c] = setup_parameters(Q,J);
for i = 1:length(intervals)
    [recovery_time_aa(i,:),peak_time_aa(i,:),peak_viral_load_aa(i,:),auc_V_aa(i,:),...
        total_T_aa(i),~,sol_aa{i}] =...
        summary_statistics(p,iv,c,V_0,intervals(i));
end

[~,~,~,~,~,~,sol_aa_100] =...
    summary_statistics(p,iv,c,V_0,fixed_interval);

% solutions for different inter-exposure intervals with cross-reactivity
% but no memory

recovery_time_aa_nomem = recovery_time_aa;
peak_time_aa_nomem = recovery_time_aa;
peak_viral_load_aa_nomem = recovery_time_aa;
auc_V_aa_nomem = recovery_time_aa;
total_T_aa_nomem = total_T_aa;
sol_aa_nomem = cell(length(intervals),1);

[p,iv,c] = setup_parameters(Q,J);
p.epsE = 0;
for i = 1:length(intervals)
    [recovery_time_aa_nomem(i,:),peak_time_aa_nomem(i,:),...
        peak_viral_load_aa_nomem(i,:),auc_V_aa_nomem(i,:),...
        total_T_aa_nomem(i),~,sol_aa_nomem{i}] =...
        summary_statistics(p,iv,c,V_0,intervals(i));
end

[~,~,~,~,~,~,sol_aa_nomem_100] =...
    summary_statistics(p,iv,c,V_0,fixed_interval);

% solutions for different inter-exposure intervals with no T cells
recovery_time_no_T = recovery_time_aa;

[p,iv,c] = setup_parameters(Q,J);
for i = 1:length(intervals)
    iv(c.C) = 0;
    [recovery_time_no_T(i,:),~,~,~,...
        ~,~,~] =...
        summary_statistics(p,iv,c,V_0,intervals(i));
end

% solutions for different inter-exposure intervals with no cross-reactivity
% but with memory

J_ab = 2;
recovery_time_ab = recovery_time_aa;
peak_time_ab = recovery_time_aa;
peak_viral_load_ab = recovery_time_aa;
auc_V_ab = recovery_time_aa;
total_T_ab = total_T_aa;
sol_ab = sol_aa;

[p,iv,c] = setup_parameters(Q,J_ab);
p.kappaE = p.kappaE.*eye(J_ab);
p.kC = p.kC./eye(J_ab);

for i = 1:length(intervals)
    [recovery_time_ab(i,:),peak_time_ab(i,:),peak_viral_load_ab(i,:),auc_V_ab(i,:),...
        total_T_ab(i),~,sol_ab{i}] =...
        summary_statistics(p,iv,c,V_0,intervals(i));
end

[~,~,~,~,~,~,sol_ab_100] =...
    summary_statistics(p,iv,c,V_0,fixed_interval);

% solutions for different inter-exposure intervals with no cross-reactivity
% and no memory

recovery_time_ab_nomem = recovery_time_aa;
peak_time_ab_nomem = recovery_time_aa;
peak_viral_load_ab_nomem = recovery_time_aa;
auc_V_ab_nomem = recovery_time_aa;
total_T_ab_nomem = total_T_aa;
sol_ab_nomem = sol_aa;

[p,iv,c] = setup_parameters(Q,J_ab);
p.kappaE = p.kappaE.*eye(J_ab);
p.kC = p.kC./eye(J_ab);
p.epsE = 0;

for i = 1:length(intervals)
    [recovery_time_ab_nomem(i,:),peak_time_ab_nomem(i,:),...
        peak_viral_load_ab_nomem(i,:),auc_V_ab_nomem(i,:),...
        total_T_ab_nomem(i),~,sol_ab_nomem{i}] =...
        summary_statistics(p,iv,c,V_0,intervals(i));
end

[~,~,~,~,~,~,sol_ab_nomem_100] =...
    summary_statistics(p,iv,c,V_0,fixed_interval);

% Figure 4
fh.plot_trajectories(intervals(1:6),strcat(dir_string,'4'),...
    sol_aa,sol_aa_nomem,sol_ab)

% Figure 5
fh.plot_summary(intervals,'Time to peak viral load (days)',1,0,...
    strcat(dir_string,'5a'),peak_time_ab(end,1)-intervals(end),...
    peak_time_aa(:,2),peak_time_aa_nomem(:,2),...
    peak_time_ab(:,2))

fh.plot_summary(intervals,'Peak viral load (virions)',0,0,...
    strcat(dir_string,'5b'),peak_viral_load_ab(end,1),...
    peak_viral_load_aa(:,2),peak_viral_load_aa_nomem(:,2),...
    peak_viral_load_ab(:,2))

fh.plot_summary(intervals,'Recovery time (days)',0,0,...
    strcat(dir_string,'5c'),recovery_time_ab(end,1),recovery_time_aa(:,2),...
    recovery_time_aa_nomem(:,2),recovery_time_ab(:,2))

fh.plot_summary(intervals,'Area under viral load curve',0,0,...
    strcat(dir_string,'5d'),auc_V_ab(end,1),...
    auc_V_aa(:,2),auc_V_aa_nomem(:,2),...
    auc_V_ab(:,2))

% Figure 6a
fh.plot_T_trajectory(strcat(dir_string,'6a'),sol_aa_100,...
    sol_aa_nomem_100,sol_ab_100,sol_ab_nomem_100)

% Figure 6b
fh.plot_summary(intervals,'Total number of T cells',1,1,...
    strcat(dir_string,'6b'),1,total_T_aa,total_T_aa_nomem,...
    total_T_ab,total_T_ab_nomem)
j = findobj(gcf);
j(2).Location = 'southeast';
save_eps_fig(gcf,strcat(dir_string,'6b'),1)

% Figure 7
fh.plot_xreact(xreact*C0_number,recovery_time_precursor_frequency(:,1),...
    recovery_time_precursor_frequency(:,2),'$C_1(0)$ (cells)',...
    'Recovery time (days)',strcat(dir_string,'7a'))
fh.plot_xreact(xreact*C0_number,iei_T_precursor_frequency,...
    total_T_precursor_frequency,'$C_1(0)$ (cells)',...
    'Total number of T cells',strcat(dir_string,'7b')) 
fh.plot_xreact(xreact*C0_number,iei_T_precursor_frequency./(xreact*C0_number),...
    total_T_precursor_frequency./iei_T_precursor_frequency,...
    '$C_1(0)$ (cells)','Expansion ratio',strcat(dir_string,'7c')) 

% Figure 8
fh.plot_xreact(xreact,recovery_time_avidity(:,1),recovery_time_avidity(:,2),...
    '$a_1 (u_a)$','Recovery time (days)',strcat(dir_string,'8a'))
fh.plot_xreact(xreact,iei_T_avidity,total_T_avidity,...
    '$a_1 (u_a)$','Total number of T cells',strcat(dir_string,'8b'))
fh.plot_xreact(xreact,iei_T_avidity/C0_number,total_T_avidity./iei_T_avidity,...
    '$a_1 (u_a)$','Expansion ratio',strcat(dir_string,'8c'))

% Figure 9
fh.plot_xreact(xreact,recovery_time_abundance_change1(:,1),...
    recovery_time_abundance_change1(:,2),'$d_{11} (u_d)$',...
    'Recovery time (days)',strcat(dir_string,'9a'))
fh.plot_xreact(xreact,iei_T_abundance_change1,...
    total_T_abundance_change1,'$d_{11} (u_d)$',...
    'Total number of T cells',strcat(dir_string,'9b'))
fh.plot_xreact(xreact,iei_T_abundance_change1/C0_number,...
    total_T_abundance_change1./iei_T_abundance_change1,...
    '$d_{11} (u_d)$','Expansion ratio',strcat(dir_string,'9c'))
fh.plot_xreact(xreact,recovery_time_abundance_change2(:,1),...
    recovery_time_abundance_change2(:,2),'$d_{12} (u_d)$',...
    'Recovery time (days)',strcat(dir_string,'9d'))
fh.plot_xreact(xreact,iei_T_abundance_change2,...
    total_T_abundance_change2,'$d_{12} (u_d)$',...
    'Total number of T cells',strcat(dir_string,'9e'))
fh.plot_xreact(xreact,iei_T_abundance_change2/C0_number,...
    total_T_abundance_change2./iei_T_abundance_change2,...
    '$d_{12} (u_d)$','Expansion ratio',strcat(dir_string,'9f'))
end

% function to produce plots for sensitivity analysis
% input argument: pname: string: name of parameter to be varied
% input argument: values: vector: values which the parameter takes
% input argument: baseline: scalar: baseline parameter value in main text
function produce_plots_sensitivity(pname,values,baseline)

dir_string = strcat('results/figs_',pname,'/');

if(~exist(dir_string,'dir'))
    mkdir(dir_string);
end

Q = 2;
J = 1;
xreact = 0:.1:1;
fixed_interval = 100;
V_0 = [10,10];

C0_number = 100;
% solutions for different avidities
recovery_time_avidity = zeros(length(values),length(xreact),Q);
total_T_avidity = zeros(length(values),length(xreact),J);

for i = 1:length(xreact)
    for j = 1:length(values)
        [p,iv,c] = setup_parameters(Q,J);
        p.(pname) = values(j)*ones(size(p.(pname)));
        ad = xreact(i)*ones(Q,J);
        p.kappaE = p.kappaE.*ad;
        p.kC = p.kC./ad;
        [recovery_time_avidity(j,i,:),~,~,~,total_T_avidity(j,i,:),~,~] =...
            summary_statistics(p,iv,c,V_0,fixed_interval);
    end
end

% solutions for different precursor frequencies
recovery_time_precursor_frequency = recovery_time_avidity;
total_T_precursor_frequency = total_T_avidity;

for i = 1:length(xreact)
    for j = 1:length(values)
        [p,iv,c] = setup_parameters(Q,J);
        p.(pname) = values(j)*ones(size(p.(pname)));
        iv(c.C) = xreact(i)*C0_number;
        [recovery_time_precursor_frequency(j,i,:),~,~,~,total_T_precursor_frequency(j,i,:),~,~] =...
            summary_statistics(p,iv,c,V_0,fixed_interval);
    end
end

% solutions for different epitope abundances
recovery_time_abundance_change1 = recovery_time_avidity;
total_T_abundance_change1 = total_T_avidity;

recovery_time_abundance_change2 = recovery_time_avidity;
total_T_abundance_change2 = total_T_avidity;

for i = 1:length(xreact)
    for j = 1:length(values)
        [p,iv,c] = setup_parameters(Q,J);
        p.(pname) = values(j)*ones(size(p.(pname)));
        ad = [xreact(i);1];
        p.kappaE = p.kappaE.*ad;
        p.kC = p.kC./ad;
        [recovery_time_abundance_change1(j,i,:),~,~,~,total_T_abundance_change1(j,i,:),~,~] =...
            summary_statistics(p,iv,c,V_0,fixed_interval);
        
        [p,iv,c] = setup_parameters(Q,J);
        p.(pname) = values(j)*ones(size(p.(pname)));
        ad = [1;xreact(i)];
        p.kappaE = p.kappaE.*ad;
        p.kC = p.kC./ad;
        [recovery_time_abundance_change2(j,i,:),~,~,~,total_T_abundance_change2(j,i,:),~,~] =...
            summary_statistics(p,iv,c,V_0,fixed_interval);
    end
end

% solutions for different inter-exposure intervals with cross-reactivity
% and memory

recovery_time_aa = zeros(length(values),Q);
total_T_aa = zeros(length(values),J);

for i = 1:length(values)
    [p,iv,c] = setup_parameters(Q,J);
    p.(pname) = values(i)*ones(size(p.(pname)));
    [recovery_time_aa(i,:),~,~,~,total_T_aa(i),~,~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different inter-exposure intervals with cross-reactivity
% but no memory

recovery_time_aa_nomem = recovery_time_aa;
total_T_aa_nomem = total_T_aa;

for i = 1:length(values)
    [p,iv,c] = setup_parameters(Q,J);
    p.(pname) = values(i)*ones(size(p.(pname)));
    p.epsE = 0;
    [recovery_time_aa_nomem(i,:),~,~,~,total_T_aa_nomem(i),~,~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different inter-exposure intervals with no T cells
recovery_time_no_T = recovery_time_aa;

for i = 1:length(values)
    [p,iv,c] = setup_parameters(Q,J);
    p.(pname) = values(i)*ones(size(p.(pname)));
    iv(c.C) = 0;
    [recovery_time_no_T(i,:),~,~,~,~,~,~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different inter-exposure intervals with no cross-reactivity
% but with memory

nT_ab = 2;
recovery_time_ab = recovery_time_aa;
total_T_ab = total_T_aa;

for i = 1:length(values)
    [p,iv,c] = setup_parameters(Q,nT_ab);
    p.kappaE = p.kappaE.*eye(nT_ab);
    p.kC = p.kC./eye(nT_ab);
    p.(pname) = values(i)*ones(size(p.(pname)));
    [recovery_time_ab(i,:),~,~,~,total_T_ab(i),~,~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

% solutions for different inter-exposure intervals with no cross-reactivity
% and no memory

recovery_time_ab_nomem = recovery_time_aa;
total_T_ab_nomem = total_T_aa;


for i = 1:length(values)
    [p,iv,c] = setup_parameters(Q,nT_ab);
    p.kappaE = p.kappaE.*eye(nT_ab);
    p.kC = p.kC./eye(nT_ab);
    p.(pname) = values(i)*ones(size(p.(pname)));
    p.epsE = 0;
    [recovery_time_ab_nomem(i,:),~,~,~,total_T_ab_nomem(i),~,~] =...
        summary_statistics(p,iv,c,V_0,fixed_interval);
end

xreact = xreact';
fh = plot_fns;
switch pname
    case 'pV'
        factor_string = {'$p_V = 5.0$','$p_V = 10.1$','$p_V = 25.2$',...
            '$p_V = 50.4$','$p_V = 75.6$','$p_V = 101$'};
        x_label = '$p_V$ (virion infected cell$^{-1}$ day$^{-1}$)';
    case 'pF'
        factor_string = {'$p_F = 10^{-7}$','$p_F = 10^{-6}$',...
            '$p_F = 10^{-5}$','$p_F = 10^{-4}$'};
        x_label = '$p_F$ ($u_F$ infected cell$^{-1}$ day$^{-1}$)';
    case 'kappaA'
        factor_string = {'$\kappa_A = 3 \times 10 ^{-1}$','$\kappa_A = 3 \times 10^1$',...
            '$\kappa_A = 3 \times 10^3$','$\kappa_A = 3 \times 10^5$','$\kappa_A = 3 \times 10^7$'};
        x_label = '$\kappa_A$ ((pg/mL)$^{-1}$ day$^{-1}$)';
end

% Figure 10
fh.plot_sensitivity(values,baseline,x_label,'Recovery time (days)',...
    ~strcmp(pname,'pV'),0,strcmp(pname,'pV'),strcat(dir_string,'A10'),recovery_time_aa(:,2),...
    recovery_time_aa_nomem(:,2),recovery_time_ab_nomem(:,2),recovery_time_no_T(:,2))

% Figure 11
fh.plot_sensitivity(values,baseline,x_label,'Total no. of T cells',...
    ~strcmp(pname,'pV'),1,strcmp(pname,'pV'),strcat(dir_string,'A11'),...
    total_T_aa,total_T_aa_nomem,total_T_ab,total_T_ab_nomem)

% Figure 12
fh.plot_recovery_sensitivity_xreact(C0_number*xreact,...
    recovery_time_precursor_frequency(1:2:end,:,2),'$C_1(0)$ (cells)',...
    factor_string,1,strcat(dir_string,'A121'))
fh.plot_recovery_sensitivity_xreact(xreact,...
    recovery_time_avidity(1:2:end,:,2),'$a_1 (u_a)$',...
    factor_string,0,strcat(dir_string,'A122'))
fh.plot_recovery_sensitivity_xreact(xreact,...
    recovery_time_abundance_change1(1:2:end,:,2),'$d_{11} (u_d)$',...
    factor_string,0,strcat(dir_string,'A123'))
fh.plot_recovery_sensitivity_xreact(xreact,...
    recovery_time_abundance_change2(1:2:end,:,2),'$d_{12} (u_d)$',...
    factor_string,0,strcat(dir_string,'A124'))

end

% summary figure 3
function summary_figure(filename)

% plot bars
a = [0,3,3;0,3,3;0,10,10;14,10,14]; % left to right = bottom to top:
% recovery time, peak, shedding
b = barh(a,'grouped');
hold on
a2 = [0,0,0;0,0,0;0,0,0;10,0,0]; % hide left portion of recovery bar
b2 = barh(a2,'grouped'); 
hold off

% axis limits
h = gca;
axis([1 14 h.YLim(1) h.YLim(2)])

% assign colours
for i = 1:length(b)
    b(i).EdgeColor = 'none';
    b2(i).EdgeColor = 'none';
end

[map1,~,~] = brewermap(8,'Pastel2');
[map2,~,~] = brewermap(8,'Dark2');
map = (map1+map2)/2;
b(1).FaceColor = map(2,:);
b(2).FaceColor = map(3,:);
b(3).FaceColor = map(6,:);
b2(1).FaceColor = 'w';

% axis labels
xlabel('Inter-exposure interval (days)','Interpreter','latex','FontSize',14)
text(.9,1.1,'- cross-react','Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,.9,'- memory','Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,2.1,'- cross-react','Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,1.9,'+ memory','Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,3.1,'+ cross-react','Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,2.9,'- memory','Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,4.3,'+ cross-react',...
    'Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,4.1,'+ memory',...
    'Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,3.9,'(three',...
    'Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,3.7,'mechanisms',...
    'Interpreter','latex','HorizontalAlignment','right','FontSize',14);
text(.9,3.5,'in Sec. 3.2)',...
    'Interpreter','latex','HorizontalAlignment','right','FontSize',14);

h.YTickLabel = {'','','',''};

% text on bars
text(2,1.2,'shedding $\downarrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);
text(2,2.2,'shedding $\downarrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);
text(5.5,3.2,'shedding $\downarrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);
text(5.5,4.2,'shedding $\downarrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);

text(5.5,4,'peak time $\uparrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);
text(5.5,3,'peak time $\uparrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);
text(2,2,'peak time $\uparrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);
text(2,1,'peak time $\uparrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',14);

text(12,3.75,'recovery time $\downarrow$, T cell $\uparrow$','Interpreter','latex','HorizontalAlignment','center','FontSize',12);

% position of figure in bounding box
h.Position = [0.15,0.15,.83,.7];

% legend
leg = legend(fliplr(b),'Sec. 3.1.2, 3.1.3','Sec. 3.1.2','Sec. 3.1.1, 3.1.3');
leg.Interpreter = 'latex';
leg.Location = 'southeast';
h.FontSize = 14;

save_eps_fig(gcf,filename,1)
end

