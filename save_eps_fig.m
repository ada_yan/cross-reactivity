function save_eps_fig(fig_handle,filename,colour)
if(~isempty(filename))
    create_filename_directory(filename);
    if(colour)
        print(fig_handle,filename,'-depsc');
    else
        print(fig_handle,filename,'-deps');
    end
    saveas(fig_handle,filename,'fig');
end
end