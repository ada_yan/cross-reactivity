% collection of plotting functions
function fh = plot_fns()
addpath('brewermap')
fh_temp = localfunctions;
for i = 1:length(fh_temp)
    funcstr = func2str(fh_temp{i});
    fh.(funcstr) = fh_temp{i};
end
end

% plot viral load trajectories
% input argument: intervals: 1*n vector containing inter-exposure intervals for
% which we plot trajectories
% input argument: filename: string: base of filenames to which plots are saved
% input argument: varargin: trajectories to be plotted
% each argument in varargin is a 1*n cell array
% each of these cells constains a struct array with fields
% x: 1*p vector: timespan over which to plot trajectory
% V: 2*p vector: viral load of first and second viruses
function plot_trajectories(intervals,filename,varargin) %#ok<*DEFNU>

% define colours
[map,~,~] = brewermap(8,'Dark2');
color_array = {'k',map(4,:),map(5,:)};
grey_shaded = [224,224,224]/255;
grey_line = [.5,.5,.5];

% find YLim
max_y = 0;
space = 1.1;
for i = 1:length(intervals)
    max_y = max([max_y,varargin{1}{i}.V(1,:)]);
    for j = 1:length(varargin)
        max_y = max([max_y,varargin{j}{i}.V(2,:)]);
    end
end
max_y = 10^space*max_y;
hold off

for i = 1:length(intervals)
    % plot shaded area
    g(1) = area(varargin{1}{i}.x,varargin{1}{i}.V(1,:),'FaceColor',grey_shaded,'EdgeColor','none');
    hold on
    % plot trajectory of virus 1
    f = semilogy(varargin{1}{i}.x-intervals(i),varargin{1}{i}.V(1,:),...
        'Color',grey_line,'LineStyle',':','LineWidth',2);
    % plot trajectories of virus 2 -- manually offsetting dashes
    for j = length(varargin):-1:1
        if(mod(j,2) == 0)
            if(i == 1 || i == 2)
                g(j+1) = semilogy...
                    ([varargin{j}{i}.x(end) + .05,fliplr(varargin{j}{i}.x)]-intervals(i),...
                    [varargin{j}{i}.V(2,end)-log10(.5),fliplr(varargin{j}{i}.V(2,:))],...
                    'Color',color_array{j},'LineStyle','--','LineWidth',2);
            else
                g(j+1) = semilogy(fliplr(varargin{j}{i}.x)-intervals(i),fliplr(varargin{j}{i}.V(2,:)),...
                    'Color',color_array{j},'LineStyle','--','LineWidth',2);
            end
        else
            g(j+1) = semilogy(varargin{j}{i}.x-intervals(i),varargin{j}{i}.V(2,:),...
                'Color',color_array{j},'LineStyle','--','LineWidth',2);
        end
        hold on
    end
    hold off
    h = gca;
    h.YScale = 'log';
    h.FontSize = 20;
    xlabel('Time since second infection (days)','Interpreter','latex')
    ylabel('Viral load (virions)','Interpreter','latex')
    axis([-15 15 1 max_y])
    if(i == 1)
        leg = legend(g,'baseline','+ cross-react + memory','+ cross-react -- memory','no cross-react');
        leg.Interpreter = 'latex';
        leg.Location = 'southwest';
    elseif(i == 4)
        leg = legend(f,'primary infection');
        leg.Interpreter = 'latex';
        leg.Location = 'southwest';
    end
    title_str = strcat(num2str(intervals(i)),' day');
    if(intervals(i) > 1)
        title_str = strcat(title_str,'s');
    end
    title1 = title(title_str,'Interpreter','latex');
    title1.FontSize = 36;
    h.Position = [0.15,0.15,.7,.7];
    save_eps_fig(gcf,strcat(filename,num2str(intervals(i))),1)
end
end

% plot summary statistics varying inter-exposure interval
% input argument: intervals: 1*n vector containing inter-exposure intervals for
% which we plot summary statistics
% input argument: y_label: string: y axis label
% input argument: legend_flag: logical: whether to include legend
% input argument:log_flag: logical: whether y axis should be on log scale
% input argument: filename: string: base of filenames to which plots are saved
% input argument: varargin: summary statistics to be plotted
% each argument in varargin is a 1*n vector
function plot_summary(intervals,y_label,legend_flag,log_flag,filename,baseline,varargin)

% define colours and markers
[map,~,~] = brewermap(8,'Dark2');
if(length(varargin) == 3)
    marker_array = {'+','o','^'};
    color_array = {'k',map(4,:),map(5,:)};
else
    marker_array = {'+','o','v','^'};
    color_array = {'k',map(4,:),map(7,:),map(5,:)};
end

% find ylim
space = 1.1;
max_y = 0;
for i = 1:length(varargin)
    max_y = max([max_y;varargin{i}]);
end
axis_vector = [0 intervals(end) 0 space*max_y];
grey_shaded = [224,224,224]/255;

% plot shaded area
hold off
area([0,axis_vector(2)],repmat(baseline,1,2),'FaceColor',grey_shaded,'EdgeColor','none')
hold on
% plot lines
for i = 1:length(varargin)
    g(i) = plot(intervals,varargin{i},'Marker',marker_array{i},'Color',color_array{i},...
        'MarkerSize',10,'LineWidth',3,'LineStyle','--');
    hold on
end

hold off

h = gca;
h.FontSize = 20;

if(log_flag)
    h.YScale = 'log';
    axis_vector(3) = 1;
end
axis(axis_vector)

xlabel('Inter-exposure interval (days)','Interpreter','latex')
switch y_label
    case {'Recovery time (days)','Total number of T cells'}
        ylabel(y_label,'Color',map(2,:),'Interpreter','latex')
    case 'Time to peak viral load (days)'
        ylabel(y_label,'Color',map(3,:),'Interpreter','latex')
    case 'Area under viral load curve'
        ylabel(y_label,'Color',map(6,:),'Interpreter','latex')
    otherwise
        ylabel(y_label,'Interpreter','latex')
end
if(legend_flag)
    switch length(varargin)
        case 3
            leg = legend('baseline','+ cross-react + memory','+ cross-react -- memory','-- cross-react');
        case 4
            leg = legend(g,'+ cross-react + memory','+ cross-react -- memory',...
                '-- cross-react + memory','-- cross-react -- memory');
    end
    leg.Interpreter = 'latex';
end
h.Position = [0.15,0.15,.7,.7];
save_eps_fig(gcf,filename,1)
end

% plot trajectory of T cells
% input argument: filename: string: filename to which plot are saved
% input argument: varargin: T cell trajectories to be plotted
% each argument in varargin is a struct array with fields
% x: 1*p vector: timespan over which to plot trajectory
% T: 1*p vector: number of T cells
function plot_T_trajectory(filename,varargin)

% define colours
[map,~,~] = brewermap(8,'Dark2');
color_array = {'k',map(4,:),map(7,:),map(5,:)};

% plot trajectories
hold off
for i = 1:length(varargin)
    semilogy(varargin{i}.x,varargin{i}.T,'LineStyle','--','Color',color_array{i},'LineWidth',2)
    hold on
end
hold off
axis([0 varargin{1}.x(end) 1 1e12])
h = gca;
h.FontSize = 20;
xlabel('Time (days)','Interpreter','Latex')
ylabel('Total number of T cells','Color',map(2,:),'Interpreter','latex')
leg = legend('+ cross-react + memory','+ cross-react -- memory',...
    'no cross-react + memory','no cross-react -- memory');
leg.Interpreter = 'latex';
leg.Location = 'northwest';
h.Position = [0.15,0.15,.7,.7];
save_eps_fig(gcf,filename,1)
end

% plot summary statistic as the degree of cross-reactivity is changed
% input argument: xreact: 1*n vector: degree of cross-reactivity
% input argument: points1: 1*n vector: summary statistic for first virus
% input argument: points2: 1*n vector: summary statistic for second virus
% input argument: x_label: string: x axis label
% input argument: y_label: string: y axis label
% input argument: filename: string: filename to which plot are saved
function plot_xreact(xreact,points1,points2,x_label,y_label,filename) 
space = 1.1;
axis_array = [0 max(xreact) 0 space*max(max(points1),max(points2))];
plot(xreact,points1,'k+','MarkerSize',10,'LineWidth',3)
hold on
plot(xreact,points2,'ko','MarkerSize',10,'LineWidth',3)
hold off
h = gca;
h.FontSize = 20;
axis(axis_array)
xlabel(x_label,'Interpreter','latex')
ylabel(y_label,'Interpreter','latex')
leg = legend('$1^{st}$','$2^{nd}$');
leg.Interpreter = 'latex';
save_eps_fig(gcf,filename,0)
end

% plot summary statistic as a parameter is changed for sensitivity analysis
% input argument: x_array: 1*n vector: parameter values
% input argument: baseline_param: scalar: baseline parameter value in main
% text
% input argument: x_label: string: x axis label
% input argument: y_label: string: y axis label
% input argument: log_xflag: logical: whether x axis should be on log scale
% input argument: log_yflag: logical: whether y axis should be on log scale
% input argument: legend_flag: logical: whether to include legend
% input argument: filename: string: base of filenames to which plots are saved
% input argument: varargin: summary statistics to be plotted
% each argument in varargin is a 1*n vector
function plot_sensitivity(x_array,baseline_param,x_label,y_label,log_xflag,log_yflag,...
    legend_flag,filename,varargin)

[map,~,~] = brewermap(8,'Dark2');
% Fig. A10 and A11 have different colours and markers
switch y_label
    case 'Recovery time (days)'
        marker_array = {'+','o','^','x'};
        color_array = {'k',map(4,:),map(5,:),map(2,:)};
    otherwise
        marker_array = {'+','o','v','^'};
        color_array = {'k',map(4,:),map(7,:),map(5,:)};
end

% find Ylim
space = 1.1;
max_y = 0;
for i = 1:length(varargin)
    max_y = max([max_y;varargin{i}]);
end

if(log_xflag)
    axis_vector = [0 x_array(end) 1 10^space*max_y];    
else
    axis_vector = [0 x_array(end) 0 space*max_y];
end

% plot values
hold off
for i = 1:length(varargin)
    g(i) = plot(x_array,varargin{i},'Marker',marker_array{i},'Color',color_array{i},...
        'MarkerSize',10,'LineWidth',3,'LineStyle','--');
    hold on
end

% plot baseline values
plot([baseline_param,baseline_param],axis_vector(3:4),'k','LineWidth',3);
hold off

h = gca;
h.FontSize = 20;
axis(axis_vector)
if(log_yflag)
    h.YScale = 'log';
end
if(axis_vector(3) == 1)
    h.YScale = 'log';
end
xlabel(x_label,'Interpreter','latex')

ylabel(y_label,'Interpreter','latex')

if(legend_flag)
    switch y_label
        case 'Recovery time (days)'
            leg = legend(g,'+ cross-react + memory','+ cross-react -- memory',...
                '-- cross-react','-- T cells');
        otherwise
            leg = legend(g,'+ cross-react + memory','+ cross-react -- memory',...
                '-- cross-react + memory','-- cross-react -- memory');
            leg.Location = 'southeast';
    end
    leg.Interpreter = 'latex';
end
h.Position = [0.15,0.15,.7,.7];
save_eps_fig(gcf,filename,1)
end

% plot recovery time as a parameter is changed for sensitivity analysis
% input argument: x_array: 1*n vector: parameter values
% input argument: recovery_time: 1*n vector: recovery times
% input argument: factor_string: cell array of strings: legend contents
% input argument: legend_flag: logical: whether to include legend
% input argument: filename: string: filenames to which plot is saved
function plot_recovery_sensitivity_xreact(x_array,recovery_time,x_label,factor_string,...
    legend_flag,filename)

% define colours
line_colours = linspace(.1,.9,size(recovery_time,1))';
line_colours = repmat(line_colours,1,3);

% plot lines
for i = 1:size(recovery_time,1)
    plot(x_array,recovery_time(i,:),'x',...
        'Color',line_colours(i,:),'LineWidth',3,'LineStyle','-','MarkerSize',10)
    hold on
end
hold off

h = gca;
h.YLim = [0,min(h.YLim(2),16)];
h.FontSize = 20;
xlabel(x_label,'Interpreter','latex')
ylabel('Recovery time (days)','Interpreter','latex')
if(legend_flag)
    leg = legend(factor_string);
    leg.Interpreter = 'latex';
end
save_eps_fig(gcf,filename,1)
end
