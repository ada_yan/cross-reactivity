Repository for [Modelling cross-reactivity and memory in the cellular adaptive immune response to influenza infection in the host](https://arxiv.org/abs/1606.00495)

[produce_plots_top.m](./produce_plots_top.m) produces all plots from the manuscript.

[summary_statistics.m](./produce_plots_top.m) solves the ODE model pand produces summary statistics.